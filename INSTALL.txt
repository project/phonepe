Installation steps
------------------

  1. Enable the module - PhonePe.

  2. Configure the payment options with test credentials. Go to 'admin/config/services/payment' and configure the payment settings.
   
   * Merchant ID
   * API Key
   * Mode - Live or Dev (For testing purpose always recommended to use the dev mode. Check for the latest staging PhonePe URL and update the controller file).
  
  3. Add a new product and proceed payment. After filling the required shipping details, you will be redirectd to the PhonePe payment page.
  

