PhonePe
--------------------

PhonePe is a flexible online payment method. This module requires the following dependencies:

 * Payment
 * CHR


CONFIGURATION
-------------

 * Enable the module (PhonePe) - Administration » Modules.

 * Configure payment options in Administration » Configuration » Webservices » payment. 
     * Merchant Identifier   
     * API Key
 
 * Enable payment method - PhonePe.
 

INSTALLATION
------------

  Please read INSTALL.txt file on how to install the module.
